# Frequent cron jobs for ikiwiki-hosting-web.

# Trigger ikiwiki aggregation for sites where it is due. This runs every
# five minutes, so anyone attempting a more frequent update rate will
# be throttled back to that. (The default aggregation rate is every 15
# minutes; so a factor of 15 is desirable.)
#
# Note that aggregation jobs may stall if a remote system cannot be
# contacted or is slow. So it's desirable to run several at a time.
# They're run niced, so the main limit is the number of jobs in
# memory.
#
# Also, note that ikiwiki does its own locking to prevent a site being
# aggregated more than once at a time, so no locking is needed here.
MAXLOAD=5
MAXJOBS=5
NICE="nice -n 19 ionice -c 3"
AGGREGATE="$NICE ikisite sudo {} -- ikiwiki --setup ikiwiki.setup --aggregate --refresh -v --syslog"
*/5 * * * * root	ikisite list --aggregatedue | xargs parallel -l $MAXLOAD -j $MAXJOBS -i sh -c "$AGGREGATE" -- >/dev/null 2>&1
