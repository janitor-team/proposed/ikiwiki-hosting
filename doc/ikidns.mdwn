# NAME

ikidns - ikiwiki-hosting dns helper

# SYNOPSIS

ikidns subcommand options

# DESCRIPTION

Each web server needs to be able to update the DNS when a site is brought
up on it. They do so by talking to the master DNS server, using nsupdate(1)
and a key.

ikidns handles management of the keys, and configures the master DNS server
to accept them for each configured toplevel domain name.

# FILES

/etc/bind/$domain/db/zonefile is the zone file for a domain. A stub file
will be generated from a template if it doesn't exist, but probably needs
to be configured. As nsupdate(1) is used to add subdomains, bind
will first add them to a journal file, and later update this file to list them.

/etc/ikiwiki-hosting/ikiwiki-hosting.conf is the config file read by
default.

/etc/ikiwiki-hosting/templates/ holds templates of bind config and zone files

# EXAMPLES

	scp $(ikidns createkey $newserver) $newserver:/etc/ikiwiki-hosting/keys/dns/
	
	ikidns setupbind

# SEE ALSO

* [[ikisite]](1)

# AUTHOR

Joey Hess <joey@ikiwiki.info>

Warning: this page is automatically made into a man page via [mdwn2man](http://git.ikiwiki.info/?p=ikiwiki;a=blob;f=mdwn2man;hb=HEAD).  Edit with care
