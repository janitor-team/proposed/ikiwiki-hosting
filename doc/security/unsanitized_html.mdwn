Is it safe to allow users to put unsanitized html on their sites?

Reasons users might want to do this:

* To embed videos, etc into posts to a blog.
* To customize templates.
* To modify style sheets (not html per se, but broadly equivilant).

Note that allowing a user to git push automatically allows them to add
all sorts of files to their site. Unless something is done to lock down
regular git push, a-la-untrusted git push.

## The cookie problem

A subdomain can access cookies that belong to its parent domain. Both
reading cookies, and setting cookies. So foo.example.com could access
cookies for example.com. If example.com has something important on it,
such as a site administration interface, its session cookies could be
stolen this way.

A workaround is to make example.com hard redirect to a different domain and
only set cookies in that domain. Just redirecting to www.example.com should
do (TODO: check that browsers don't have some DWIM special case for `www`).
Or use example.net, or admin.example.com, etc. A subdomain *cannot* access
cookies belonging to sibling subdomains.

## TODO

This analysis is not complete; what about other means of attack beyond
cookies?
