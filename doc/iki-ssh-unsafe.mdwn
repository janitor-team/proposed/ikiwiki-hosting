# NAME

iki-ssh-unsafe - ikiwiki-hosting helper

# SYNOPSIS

iki-ssh-unsafe [ssh options]

# DESCRIPTION

This is a wrapper for ssh, and the parameters are passed
on to that program. Strict host key checking is disabled.

# SEE ALSO

* [[ikisite]](1)
* [[ssh]](1)

# AUTHOR

Joey Hess <joey@ikiwiki.info>

Warning: this page is automatically made into a man page via [mdwn2man](http://git.ikiwiki.info/?p=ikiwiki;a=blob;f=mdwn2man;hb=HEAD).  Edit with care
