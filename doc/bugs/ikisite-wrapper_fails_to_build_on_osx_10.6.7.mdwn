I decided to just give ikiwiki-hosting a spin on my mac for evaluation
purposes for now, and it currently fails to build out of the box for me,
here's a small patch to let it build and the tests pass as well, but it is
not a good and general clearenv() subsitute. I've yet to experiment with
the software.

> I'm a bit surprised/confused by this being necessary, since ikiwiki 
> itself uses clearenv in its wrapper. Why does the wrapper's C code
> that runs clearenv apparently work on OSX, but not the code here?
> 
> Ah, I see, ikiwiki only uses clearenv in tcc mode, while manipulating
> environ directly otherwise, and that is said to work on freebsd, so
> probably also OSX.  I've done the same thing here now, let me know if it
> doesn't work. [[done]] --[[Joey]] 

<pre>
From eee70a464c9a577f69b0e3a1cafec579c398eb12 Mon Sep 17 00:00:00 2001
From: Jimmy Tang <jtang@tchpc.tcd.ie>
Date: Sun, 29 May 2011 16:36:45 +0100
Subject: [PATCH] clearenv() is not available on OSX 10.6.7

This patch is offers a simple clearenv alternative on OSX, and is
directly taken from

http://svn.deepdarc.com/code/miredo-osx/tags/prerelease-1/miredo/compat/clearenv.c

It is only used when built on OSX.
---
 ikisite-wrapper.c |   15 +++++++++++++++
 1 files changed, 15 insertions(+), 0 deletions(-)

diff --git a/ikisite-wrapper.c b/ikisite-wrapper.c
index f966515..8b64468 100644
--- a/ikisite-wrapper.c
+++ b/ikisite-wrapper.c
@@ -18,7 +18,13 @@
 #include <sys/types.h>
 #include <string.h>
 
+#ifdef __APPLE__
+# include <crt_externs.h>
+# define environ (*_NSGetEnviron())
+int clearenv(void);
+#else
 extern char **environ;
+#endif
 
 int main (int argc, char **argv) {
        int i;
@@ -78,3 +84,12 @@ int main (int argc, char **argv) {
        perror(args[0]);
        exit(1);
 }
+
+
+#ifdef __APPLE__
+int clearenv(void)
+{
+       *environ = NULL;
+       return 0;
+}
+#endif
-- 
1.7.5.2
</pre>
