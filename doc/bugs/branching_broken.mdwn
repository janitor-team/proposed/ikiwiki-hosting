Noticed some breakage in handling branching of existing sites.

* Branching an external site took over its internal dns.
* Some things in ikiwiki.setup remained set to values for the parent site.

[[done]]
