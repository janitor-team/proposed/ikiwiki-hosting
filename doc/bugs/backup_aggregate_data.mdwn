The [[design/backupformat]] does include `.ikiwiki/aggregate`
so  sites using aggregation, when restored, will think everything
has been aggregated already, and will (probably, need to check) not
re-create the aggregated files, which are *not* included.

Should the aggregate state file be included in the backup? If so,
what about the source files aggregate stores, which are not in git by
default, and so also not included in the backup?

[[done]], not including aggregate in backup. Only downside really is that
on restore a site won't have aggregated data for 5 minutes until the cron
job triggers re-aggregation.
