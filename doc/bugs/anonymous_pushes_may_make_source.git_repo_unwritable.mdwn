I am not sure the circumstances which allowed this to happen

~~~~
r-wiki@marcos:~/source.git$ ls -al objects/
total 48
drwxrwxr-x 12 r-wiki       r-wiki       4096 avr 12 10:21 .
drwxrwsr-x  7 r-wiki       r-wiki       4096 avr 13 07:57 ..
drwxrwsr-x  2 ikiwiki-anon ikiwiki-anon 4096 avr  8 08:41 09
drwxrwsr-x  2 r-wiki       r-wiki       4096 avr 12 10:21 41
drwxrwsr-x  2 r-wiki       r-wiki       4096 avr 12 10:21 56
drwxrwsr-x  2 r-wiki       r-wiki       4096 avr 12 10:21 5d
drwxrwsr-x  2 r-wiki       r-wiki       4096 avr 12 10:21 6b
drwxrwsr-x  2 r-wiki       r-wiki       4096 avr 12 10:21 8b
drwxrwsr-x  2 r-wiki       r-wiki       4096 avr 12 10:21 9e
drwxrwsr-x  2 r-wiki       r-wiki       4096 avr 12 10:21 f3
drwxrwxr-x  2 r-wiki       r-wiki       4096 avr 13 07:57 info
drwxrwxr-x  2 r-wiki       r-wiki       4096 avr 13 07:57 pack
~~~~

But it broke editing the wiki from the web, in the sense that the history didn't get push to the `source.git` directory, which in turns means that synchronised wikis are not updated and other hooks not ran:

~~~~
r-wiki@marcos:~/source$ git push
Counting objects: 11, done.
Delta compression using up to 2 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (8/8), 1.98 KiB, done.
Total 8 (delta 5), reused 5 (delta 2)
error: insufficient permission for adding an object to repository database ./objects

fatal: failed to write object
error: unpack failed: unpack-objects abnormal exit
To /home/r-wiki/source.git
 ! [remote rejected] master -> master (n/a (unpacker error))
error: failed to push some refs to '/home/r-wiki/source.git'
~~~~

I think the problem is someone pushed a blob that had the first `09` hash prefix through the git daemon, while all the other hash prefix directories have been created by the web interface. The workaround I found is:

    chmod g+s objects
    chown :r-wiki 09 09/*

... but that's hardly convenient. :) -- [[anarcat]]

> `ikiwiki-anon` is the user used for anonymous git push.
> See "permissions mess" in [[todo/anonymous_git_push]] for a discussion of
> this problem, and its solution, which is to use a filesystem supporting
> ACLs. `ikisite enable` runs an appropriate `setfacl` for you,
> and warns if it fails. We use this at Branchable and it seems to work
> fine. --[[Joey]]

> > Awesome, so just adding acl to the mount point (add installing the acl package, but I already had that :) fixes the problem, after running `ikisite enable` again on the site, thanks and [[done]]! --[[anarcat]]
