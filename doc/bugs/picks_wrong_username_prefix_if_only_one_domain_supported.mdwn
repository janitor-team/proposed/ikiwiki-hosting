Suppose you have this:

    # List supported domains here.
    domains=example.com
    
    # Username prefixes used to ensure unique usernames are used for
    # sites under different top level domains. Keep the prefixes short!
    prefix_ih=example.com

In `username()` we do this:

	# add unique prefix to avoid collisions with system users and 
	# other top domains
	my $prefix;
	foreach my $key (keys %config) {
		next unless defined $config{$key};
		next unless $config{$key} eq $topdomain;
		$prefix=$key;
		$prefix=~s/^prefix_//;
		last;
	}

The value of `domains` is defined and equals `example.com`, so if it happens
to come first in the hash (randomized order), ikisite will set
`$prefix=domains`, leading to usernames like `domains-foo` instead of the
intended `ih-foo`. At least in unstable, this happens inconsistently
(again, randomized hash order) so I ended up with `foo.example.com`
creating both `ih-foo` and `domains-foo`, and distributing its files
arbitrarily between the two.

The solution is simple: `next unless $key =~ m/^prefix_/;`

Fixed in my `ready/prefix` [[branch|patch]]:
<http://git.pseudorandom.co.uk/smcv/ikiwiki-hosting.git/shortlog/refs/heads/ready/prefix>

--[[smcv]]

> [[fixed|done]] --[[Joey]]
