[[todo/external_hostnames]] need to be 
validated when changed -- we'll want to ensure that
the DNS is properly set up to point to the site hostname. We'll probably
want to at least warn about hardcoded IPs.

Validation will also help ensure that two sites don't try to generate
conflicting apache config files, but by itself it's not sufficient.
Consider:

* The user setups up a CNAME example.com pointing to site A.
* The user adds example.com to site A.
* The dns is changed to example.com now points to site B.
* The user adds example.com to site B.

Now ikisite will be generating conflicting apache config files. This
scenario needs to be caught somehow, and doing so seems to call for a
site-wide database.

----

On the ikisite side, I think a new subcommand is needed that handles
setting external hostnames. It can do some of the validation. It is also
needed to allow changing/adding an external hostname and adding/removing
apache config files w/o having to 'ikisite disable' first, and
'ikisite enable' afterwards.

----

Update: `ikisite domains` sets up external hostnames/aliases, and 
does check the DNS. It also uses `ikisite sitelookup` to ensure that 
no other site is already configured to use the same external hostname.

While `ikisite sitelookup` still doesn't support multiserver use,
I think what I did today is enough to call this bug [[done]].
