`iki-git-hook-update` limits what pushes from external are allowed to do,
which is necessary, but it may limit too much. It does not allow
adding new branches, deleting branches; may not allow manipulating
tags (need to check).

It makes sense to not allow any modification of the setup branch,
and it should prevent outright deletion of the master branch.
I think all other branch changes should be safe, and allowed. --[[Joey]] 

> [[fixed|done]]. Note that for untrusted git push, ikiwiki's own checks
> will disallow changes to any branch other than master.
