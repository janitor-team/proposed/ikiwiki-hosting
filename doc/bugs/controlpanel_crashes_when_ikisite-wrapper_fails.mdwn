If ikisite-wrapper is not suid (which is the default), the command will fail when ran. Or more precisely, it will return nothing.

This will make the following code block crash:

[[!format perl """
        foreach my $siteinfo (@{IkiWiki::Hosting::yamlgetshell(
                                  "ikisite-wrapper", "list", "--extended",
                                  (map { "--admin=$_"} @accounts),
                                  (map { "--owner=$_"} @accounts),
                              )}) {
"""]]

... in controlpanel.pm, because it returns an empty reference. I'm  not sure what to do with this - as I'm not really using the control panel, but I figured this could help:

[[!format diff """
diff --git a/IkiWiki/Plugin/controlpanel.pm b/IkiWiki/Plugin/controlpanel.pm
index c55a97d..37981f1 100644
--- a/IkiWiki/Plugin/controlpanel.pm
+++ b/IkiWiki/Plugin/controlpanel.pm
@@ -35,11 +35,12 @@ sub sessioncgi ($$) {
 
        my (@owned_sites, @adminned_sites);
        my $num=0;
-       foreach my $siteinfo (@{IkiWiki::Hosting::yamlgetshell(
-                                 "ikisite-wrapper", "list", "--extended",
-                                 (map { "--admin=$_"} @accounts),
-                                 (map { "--owner=$_"} @accounts),
-                             )}) {
+       my $sites = IkiWiki::Hosting::yamlgetshell(
+           "ikisite-wrapper", "list", "--extended",
+           (map { "--admin=$_"} @accounts),
+           (map { "--owner=$_"} @accounts),
+           );
+       foreach my $siteinfo (@{$sites}) {
                $siteinfo->{cgiurl}=$config{cgiurl};
                $siteinfo->{num}=$num++;
 
"""]]


... no? --[[anarcat]]

> ping! there's a [[!taglink patch]] here. :) -- [[anarcat]]
