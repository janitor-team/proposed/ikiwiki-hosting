Open todo items (link fixed ones to [[done]]):

[[!inline pages="todo/* and !todo/*/* and !link(todo/done) and !todo/done"
archive=yes rootpage="todo"]]
