ikiwiki-hosting 0.20161219 released with [[!toggle text="these changes"]]
[[!toggleable text="""
 * [ Joey Hess ]
   * Initial support for Lets Encrypt.
   * The use\_letsencrypt setting can be set for a site by running
     ikisite letsencrypt domain, and it will attempt to get the certificate
     for it using certbot.
   * ikisite domains: Update certificate using certbot when set of domains
     changes.
   * Added ikisite maintaincerts to request/renew Lets Encrypt certs as needed,
     and added it to the daily cron job.
   * The files /etc/ikiwiki-hosting/config/$username/domain.{crt,key,chain}
     are used, when they exist, in preference to the files
     /etc/ikiwiki-hosting/config/$username/ssl.{key,crt}. This allows
     a site with multiple domains to have different certificates
     for them. The Lets Encrypt support uses this."""]]