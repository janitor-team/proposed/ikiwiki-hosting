ikiwiki-hosting 0.20180719 released with [[!toggle text="these changes"]]
[[!toggleable text="""
 * [ Joey Hess ]
   * ikisite: Deleting per-domain letsencrypt cert when a wildcard cert
     exists was too dangerous and buggy, including sometimes deleting the
     letsencrypt wildcard cert. Removed that behavior; any per-domain cert
     will be used in preference to the wildcard cert.
   * Further fix to IkiWiki::Hosting for syslog name change.
     (Fixes ikidns)
   * ikidns: Fix typo in letsencrypt command.
 * [ Simon McVittie ]
   * debian: Pass dpkg-buildflags CFLAGS to make
   * debian: Override dh\_missing to detect any files that are installed by
     dh\_auto\_install but not packaged"""]]