ikiwiki-hosting 0.20170622 released with [[!toggle text="these changes"]]
[[!toggleable text="""
 * [ [[Joey Hess|joey]] ]
   * remove, letsnotencrypt: Remove Lets Encrypt renewal file, to avoid
     the cron job trying to renew deleted sites.
   * Fix deletion of sites that use https over the web interface.
   * HTTP Strict Transport Security (HSTS) is enabled for all
     sites that have `redirect_to_https` set in their configuration.
     Thanks, Antoine Beaupré.
   * Improve `ikisite backup` to lock the wiki for a much shorter period of time.
   * Remove `.ikiwiki/sessions.db` from the ikisite backup, as the file can be
     rather large, and losing it only means users have to log back in sooner
     than would otherwise be the case.
   * ikisite-wrapper: Allow `ikisite enable` to be run via the wrapper.
     The CGI uses this to update the site config of an already enabled site
     when enabling eg `redirect_to_https` or adding a DNS alias.
 * [ [[Simon McVittie|smcv]] ]
   * debian/copyright: Use preferred https URL for Format
   * debian/control: Declare compliance with Debian Policy 4.0.0
   * debian: Update to debhelper compat level 10"""]]
