Branchable's ssh host keys are now published on Monkeysphere.
<http://www.branchable.com/forum/verifying_ssh_host_keys/>

However, there is
a limitation. Monkeysphere will only see the key if the user connects to
`branchable.com`, not to `sitename.branchable.com`.

We'd really like to be able to tell monkeysphere that "*.branchable.com"
can use any of our ssh host keys. But I don't think it supports
wildcarding.

An alternative would be to do `monkeysphere-host add-servicename` for each
subdomain we set up. But when they're all on the same key like that 
a) the key will tend to get *big* and b) the key will have a list of
every one of our subdomains, which is not information we want to expose.

The other approach would be to run `monkeysphere-host import-key` for each
subdomain we set up, so each has a unique key. But that has problems also:
a) we'd have to keep a gpg key on our server to sign those keys, and it
would have to be put in the web of trust -- I don't like keeping gpg keys
on network servers. b) It might *still* allow getting a list of all
our subdomains, if you can get ahold of the entire worldwide gpg web of
trust.

The final option would be for us to recommend users use ssh with
`branchable.com`, not a subdomain. And, behind the scenes, redirect it to
the appropriate server for the user who is logging in.

---

Eventually, I want to support using monkeysphere for user's ssh key configuration.

This would allow the site owner to enter an email address, and have ssh keys
for that user's email automatically retrieved. To help the site owner make
sure the ssh key was signed by the right gpg key, it could display a partial
web of trust. (Or, if we knew the user's own gpg key, we could do a better
display).

Revocations of configured keys could also be scanned for, and the keys
disabled. --[[Joey]] 

> Great idea! Documentation to set something like this up is [[the Monkeysphere SSH admin guide|http://web.monkeysphere.info/getting-started-admin/]], see "Monkeysphere for user authentication". --[[anarcat]]

---

Another use for monkeysphere is in distributing the per-site ssh user keys
used for automatic git pushes. Should be easy enough to commit those keys
right into monkeysphere when they're generated.
