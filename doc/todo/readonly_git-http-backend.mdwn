Git cloning from http://source.site.example.com/ rather than using git://
is valuable to some users. 

Of course, the branchable setting would enable/disable this.
