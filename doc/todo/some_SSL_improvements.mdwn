Please consider my `ready/ssl` [[branch|patch]]:
<http://git.pseudorandom.co.uk/smcv/ikiwiki-hosting.git/shortlog/refs/heads/ready/ssl>

Changes are:

* Include /etc/ikiwiki-hosting/b-foo/apache-ssl.conf.tmpl in the SSL
  vhost, but not the unencrypted vhost. This is a good place to configure
  HTTP basic/digest authentication or adjust SSL ciphers, for instance.

* Similarly, for completeness, include
  /etc/ikiwiki-hosting/b-foo/apache-source.conf.tmpl in the
  source.foo vhost.

* apache.conf.tmpl is included in all vhosts (unencrypted, SSL and source)
  as before.

* Give sites a boolean redirect_to_https option. If on, the normal port-80
  vhost behaves like the aliases, redirecting to the SSL vhost.

* If a SSL key exists, but redirect_to_https is not set, unencrypted aliases
  redirect to the unencrypted main site (but SSL aliases redirect to the
  SSL site).

That last change makes it much more palatable to have a public,
mostly-read-only site that doesn't need SSL for normal use, but install a
self-signed or otherwise non-cartel-approved certificate so that a few
authorized editors (who can be taught to verify the self-signed cert by
fingerprint) can use password authentication securely. For instance,
that's probably what I'm going to do for my blog.

Truth table: suppose www.example.com is the canonical/preferred name of
example.com, aka example.branchable.com.

    redirect from  |r_t_https=0  |r_t_https=1  | old behaviour
    ---------------+-------------+-------------+-------------
    http://www.e.c |(no redirect)|https://www  |(no redirect)
    https://www.e.c|(no redirect)|(no redirect)|(no redirect)
    http://e.c     |http://www   |https://www  |https://www
    https://e.c    |https://www  |https://www  |https://www
    http://e.b.c   |http://www   |https://www  |https://www
    https://e.b.c  |https://www  |https://www  |https://www

--[[smcv]]

> [[merged|done]] --[[Joey]]
