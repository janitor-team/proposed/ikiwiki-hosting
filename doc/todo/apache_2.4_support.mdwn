apache 2.4 has some changes in Debian which makes ikiwiki-hosting struggle. at the very least the filenames in `sites-available` need to change to have a `.conf` suffix, but there may be more I'm missing.

i have a (so far) working [[patch]] in my git repo (`git://src.anarc.at/ikiwiki-hosting`) branch `dev/apache24`. --[[anarcat]]

> I think your patch is going to break Apache 2.2 users (like, er,
> branchable.com) - I think it's OK to write out `$apache_site.conf`
> unconditionally, but then you have to use `a2ensite $apache_site.conf`
> for 2.2 or `a2ensite $apache_site` for 2.4.
>
> It would also be nice to clean up the old files automatically.
>
> Lower-priority, it would be nice to avoid the deprecated (and somewhat
> confusing) Order, Allow and Deny directives for 2.4, which could be
> achieved like so:
>
>     <IfVersion < 2.4>
>     Order allow,deny
>     allow from all
>     </IfVersion>
>     <IfVersion >= 2.4>
>     Require all granted
>     </IfVersion>
>
> or by using the template engine.
>
> I've opened an equivalent bug in Debian so it can be on the
> Debian Apache packagers' radar. --[[smcv]]
>
> Please consider my `ready/apache24` branch.
> <http://git.pseudorandom.co.uk/smcv/ikiwiki-hosting.git/shortlog/refs/heads/ready/apache24>
>
> It does not handle migration from ikisite-example.com to
> ikisite-example.com.conf at the time you install Apache 2.4: that can be done
> by disabling and re-enabling all sites, if Apache 2.4 doesn't get an
> automatic migration in its maintainer scripts before jessie is released
> (which seems to be the plan). However, it does work with both 2.2 and 2.4.
> --[[smcv]]

>> What if I have an apache 2.2 host that has lots of existing sites 
>> configured with `$apache_site`? This patch seems to cause them to break
>> in some way or another until I disable/reenable them all.
>>
>>> I might have misunderstood what you're getting at here: do you mean
>>> "I have an Apache 2.2 host and switch it to smcv's patched
>>> ikiwiki-hosting while keeping Apache 2.2", or "I have an Apache 2.2
>>> host and switch it to Apache 2.4"?
>>>
>>> The former worked for me... I'm still using Apache 2.2 on the server
>>> that I'm interested in upgrading. Old sites remain in the old,
>>> Apache-2.2-only scheme; when I disable and re-enable a site,
>>> it's moved to the new scheme, which works either way.
>>> If you've tried this and encountered a bug, or spotted a bug
>>> via code review, please explain?
>>>
>>> The latter is not (yet) expected to work, unless you disable and
>>> re-enable all ikisites (which I wanted to avoid for now, because
>>> I think some of mine still have local edits, although merging my
>>> ssl branch hopefully removed the need for that). It looks as though
>>> the Apache 2.4 maintainer scripts might do it, one day. It would
>>> maybe be reasonable to add an "ikisite migrate" subcommand,
>>> which either does the disable/enable for every site or does a
>>> more minimal rename-the-configurations step, and make
>>> ikiwiki-hosting-web's maintainer script run it - would you like me to
>>> try that? --s
>>
>> Did you consider just making ikisite write out both `$apache_site` and 
>> `apache_site.conf`? Then the runtime complication of needing to 
>> `a2ensite $apache_site.conf` on 2.2 goes away. --[[Joey]]

>>> Maybe. I was worried that when the Apache maintainers get round to this:
>>>
>>>     #XXX: Deal with the sites-available/sites-enabled *.conf transition, e.g. rename
>>>     #     all files which look like site configuration?
>>>
>>> that process could get broken by having both ikisite-example.com and
>>> ikisite-example.com.conf in sites-available. --s

>>>> Ok, this seems acceptable and I'm ok with punting handling upgrades to
>>>> 2.4 yet, in hopes apache will deal with it. [[merged|done]] --[[Joey]] 
