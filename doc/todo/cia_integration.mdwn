Would be nice if changes to sites could be communicated to cia.vc.

This is similar to auto-pushing changes to
[[git_remotes|git_remotes_configurator]] except of course CIA's nasty
hook scripts have to be used, and some of their configuration exposed
to the user.
