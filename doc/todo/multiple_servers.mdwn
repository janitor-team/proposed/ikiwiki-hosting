Eventually, we will want multiple web servers, each with their own set of
sites.

# Known multiple server issues

* `ikisite siteexits` only checks sites on the local server, would need
  to use dns or other means to check for sites elsewhere
* `ikisite sitelookup` also only checks local sites.
* If servers have different ssh host keys, then users would get scary
  prompts if a site moved to a different server. Could be avoided by using
  a single host key for all (but how secure is that?)
  (ssh's warning message about the IP changing is not scary)
  Alternate approach would be to proxy all ssh connections through a central
  server, which is IIRC what github does.
  (Monkeysphere is a way to distribute ssh keys to users, but only if they
  use it.)
* DNS ttl issues when moving a site to a different host.
* Hardcoded IPs in users' DNS prevent moving some sites around.
* `ikisite list` only lists local sites, and is used by controlpanel etc.

We may want to mirror some sites to multiple servers, too.

> One thing I am thinking of is that we could use CNAME records instead of A to point the domains to the relevant servers. Then it would be up to the admin to configure his DNS properly, and add the list of available servers to the configuration. This would fix the problem of hardcoded IPs, of different host keys (because the SSH hostname would simply change), and so on. I certainly already feel unconfortable with all those A records in my zones already: IP renumbering is then unreliable and flaky... -- [[anarcat]]
