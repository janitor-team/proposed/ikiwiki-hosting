Need to support domain names that the user has configured to CNAME to 
the site hostname (or ugh, has hardcoded our IP).

Per the [[design]], each site has a primary url, which may be
an external domain or one we assign. It may also have any number of
aliases.

## ikiwiki configuration

Set url, cgiurl to use the primary url. (do something about historyurl and
diffurl too, or will they point at a different domain?)

## apache configuration

Serve requests in the primary domain.

For alias domains, hard redirect to the page in the primary domain?
The other option would be to also serve pages under the alias domains,
which would work, but since ikiwiki uses some absolute urls (ie, the cgiurl),
the user would tend to bounce over to the primary domain eventually anyway.

Probably it's better for caching, link visited status, and even
security, for only one domain to really be used.

## data storage

(The primary url is best stored as the url field in ikiwiki's config.)

The list of aliases needs to be stored somewhere. This could be in a
separate [[design/database]], or it could be in ikiwiki's own config. 

If in ikiwiki's own config, it may or may not be editiable via websetup,
depending on whether we have granted a given user the ability to use
external domain names.

Currently, I am storing it using the ikiwikihosting plugin's urlalias setup file
setting.

[[done]] (mostly)
