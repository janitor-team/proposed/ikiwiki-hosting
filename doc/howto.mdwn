Shell recipes for various things.

## Running a command on all sites

Sometimes you have a ikisite command that needs to be run on every site.
For example, adding a new setup option. No problem:

	for s in $(ikisite list); do ikisite foo $s ; done

## Parking a site

A parked site has all remote access to its content disabled, and replaced
with a vague message saying it is unavailable. This can be useful if there
was a legal or payment problem, or to reserve a particular site name.

	sudo ikisite changesetup test.branchable.com --enable-plugin parked --rebuild

Optionally, include `--set parked_message="reason why it was parked"`

## Unparking a site

	sudo ikisite changesetup test.branchable.com --disable-plugin parked --rebuild

