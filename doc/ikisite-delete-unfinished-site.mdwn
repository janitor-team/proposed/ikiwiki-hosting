# NAME

ikisite-delete-unfinished-site - ikiwiki-hosting helper

# SYNOPSIS

ikisite-delete-unfinished-site username|--all [--dry-run]

# DESCRIPTION

This helper script is run by by cron daily,
and deletes sites that someone started to create, but never finished
setting up.

# SEE ALSO

* [[ikisite]](1)

# AUTHOR

Joey Hess <joey@ikiwiki.info>

Warning: this page is automatically made into a man page via [mdwn2man](http://git.ikiwiki.info/?p=ikiwiki;a=blob;f=mdwn2man;hb=HEAD).  Edit with care
