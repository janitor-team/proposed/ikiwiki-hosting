#!/usr/bin/perl
# Finds sites whose registration was not completed, and deletes
# after a timeout period.

my $one_day=24 * 60 * 60;
my $timeout=2 * $one_day;

no lib '.';
use warnings;
use strict;
use IkiWiki::Hosting;
use Getopt::Long;

sub usage {
	die "usage: $0 username|--all [--dry-run]\n";
}

my $all=0;
my $dry_run=0;
GetOptions(
	"all" => \$all,
	"dry-run" => \$dry_run,
) || usage();

IkiWiki::Hosting::readconfig();

if ($> != getpwnam("root")) {
	die "$0 must be run by root\n";
}

my @siteinfo;
if ($all) {
	usage() if @ARGV;
	@siteinfo=(@{IkiWiki::Hosting::yamlgetshell(
		"ikisite", "list", "--extended")});
}
else {
	usage() unless @ARGV;
	foreach my $username (@ARGV) {
		push @siteinfo, (@{IkiWiki::Hosting::yamlgetshell(
			"ikisite", "list", "--extended",
			"--owner=$username")});
	}
}

foreach my $site (@siteinfo) {
	next unless $site->{isunfinished};

	# Skip sites just created, to avoid deleting a site a user is still
	# setting up.
	if ($site->{site_created} + $timeout > time()) {
		if ($site->{site_created} + $timeout <= time() + $one_day) {
			print "tomorrow, will delete unfinished site ".$site->{site_hostname}."\n";
		}
		next;
	}

	print "deleting unfinished site ".$site->{site_hostname}."\n";
	if (! $dry_run) {
		IkiWiki::Hosting::shell("ikisite", "delete",
			$site->{site_hostname})
	}
}
