/*
 * git update hook that allows trusted sources to make any change,
 * while untrusted sources may only push changes that seem safe.
 *
 * (iki-git-shell sets UNTRUSTED_COMMIT=1 for commits pushed from external
 * sources)
 *
 * In C for speed.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main (int argc, char **argv) {
	char *untrusted=getenv("UNTRUSTED_COMMIT");

	if (argc != 4) {
		fprintf(stderr, "expected: <ref> <oldrev> <newrev>\n");
		exit(1);
	}

	if (! untrusted || strcmp(untrusted, "1") != 0) {
		exit(0);
	}

	if (strcmp(argv[3], "0000000000000000000000000000000000000000") == 0) {
		/* don't allow deletion of master branch or the setup
		 * branch, which would hose the site */
		if (strcmp(argv[1], "refs/heads/master") == 0 ||
		    strcmp(argv[1], "refs/heads/setup") == 0) {
			fprintf(stderr, "you are not allowed to delete %s\n", argv[1]);
			exit(1);
		}
	}

	/* Use ikisite to check modifications of the setup branch. */
	if (strcmp(argv[1], "refs/heads/setup") == 0) {
		unsetenv("GIT_DIR");
		execlp("ikisite", "ikisite", "checksetup", argv[3], NULL);
		perror("ikisite");
		exit(1);
	}

	exit(0);
}
